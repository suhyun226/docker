import subprocess

def execute(command):
    proc = subprocess.Popen(command, shell=True, encoding='cp949',
        stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    ret, err = proc.communicate()
    return ret


ret = execute('docker ps --all --format "{{.ID}}"'.split())
containers = ' '.join(ret.split('\n'))
execute(f'docker stop {containers}')
execute(f'docker rm {containers}')