from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import HttpResponse
from django.contrib.auth.models import User
from .models import Board

def index(request):
    return render(request, 'index.html')

def userJoin(request):
    if request.method == 'GET':
        return render(request, 'registration/join.html')
    elif request.method == 'POST':
        # 회원가입에 필요한 최소 정보 | 추가 정보
        # 계정명, 패스워드            | 이메일, 성, 이름
        user_exists = User.objects.filter(username=request.POST.get('username')).count()
        pass_check = 0
        if user_exists == 0:
            if request.POST.get('password') == request.POST.get('pass_chk'):
                data = User.objects.create_user(
                    request.POST.get('username'),
                    request.POST.get('email'),
                    request.POST.get('password'),
                    first_name=request.POST.get('first_name'),
                    last_name=request.POST.get('last_name')
                )
                return redirect('/')
            pass_check = 1

        # 패스워드 체크에 문제가 있을 때 및 기존에 가입된 사용자가 있을 때
        context = {
            'data': request.POST,
            'user_exists': user_exists,
            'pass_check': pass_check
        }
        return render(request, 'registration/join.html', context)

def boardIndex(request):
    data = Board.objects.all().order_by('-id')  # id를 기준으로 역순 정렬
    context = {
        'data': data
    }
    return render(request, 'board/index.html', context)

def boardWrite(request):
    if request.user.is_authenticated:
        if request.method == 'GET':
            return render(request, 'board/write.html')
        elif request.method == 'POST':
            data = Board()
            data.title = request.POST.get('title')
            data.content = request.POST.get('content')
            data.author = request.user.username
            data.save()

            return redirect(f'/board/view/{data.id}/')
    else:
        return redirect('/login/')

def boardUpdate(request, board_id):
    if request.user.is_authenticated:
        data = Board.objects.get(id=board_id)
        if request.user.username == data.author:
            if request.method == 'GET':
                context = {
                    'data': data
                }
                return render(request, 'board/update.html', context)
            elif request.method == 'POST':
                data.title = request.POST.get('title')
                data.content = request.POST.get('content')
                data.save()
                return redirect(f'/board/view/{data.id}/')
        else:
            # return redirect('/error/permission/')
            return HttpResponse('수정 할 권한이 없습니다.')
    else:
        return redirect('/login/')

def boardDelete(request, board_id):
    if request.user.is_authenticated:
        data = Board.objects.get(id=board_id)
        if request.user.username == data.author:
            if request.method == 'GET':
                context = {
                    'data': data
                }
                return render(request, 'board/delete.html', context)
            elif request.method == 'POST':
                data.delete()
                return redirect('/board/')
        else:
            # return redirect('/error/permission/')
            return HttpResponse('삭제할 권한이 없습니다.')
    else:
        return redirect('/login/')

def boardView(request, board_id):
    data = Board.objects.get(id=board_id)

    if not request.COOKIES.get('view'):
        data.view_cnt += 1
        data.save(update_fields=('view_cnt',))

    context = {
        'data': data
    }

    response = render(request, 'board/view.html', context)
    response.set_cookie('view', True, max_age=60*60*24*14, path=f'/board/view/{data.id}/')
    return response

def boardGoodBad(request, board_id):
    if request.user.is_authenticated:
        data = Board.objects.get(id=board_id)
        
        if not request.COOKIES.get('good_bad'):
            if request.GET.get('flag') == 'good':
                data.good_cnt += 1
                data.save(update_fields=('good_cnt',))
            elif request.GET.get('flag') == 'bad':
                data.bad_cnt += 1
                data.save(update_fields=('bad_cnt',))

        response = redirect(f'/board/view/{data.id}')
        response.set_cookie('good_bad', True, max_age=60*60*24*14, path=f'/board/good_bad/{data.id}/')
        return response
    else:
        return redirect('/login/')