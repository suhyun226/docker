"""mycodey URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import include
from . import views

urlpatterns = [
    path('', views.index),
    path('', include('django.contrib.auth.urls')),
    path('join/', views.userJoin),
    path('board/', views.boardIndex),
    path('board/write/', views.boardWrite),
    path('board/update/<int:board_id>/', views.boardUpdate),
    path('board/delete/<int:board_id>/', views.boardDelete),
    path('board/view/<int:board_id>/', views.boardView),
    path('board/good_bad/<int:board_id>/', views.boardGoodBad),
    path('admin/', admin.site.urls),
]
