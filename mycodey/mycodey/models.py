from django.db import models

class Board(models.Model):
    title = models.CharField(max_length=250)
    author = models.CharField(max_length=50)
    content = models.TextField()
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    view_cnt = models.IntegerField(default=0)
    good_cnt = models.IntegerField(default=0)
    bad_cnt = models.IntegerField(default=0)